package backend

import (
	"bytes"
	"reflect"
	"testing"
)

func TestNewGoBackupsBackend(t *testing.T) {
	type args struct {
		configFileName string
	}
	tests := []struct {
		name string
		args args
		want *GoBackupsBackend
	}{
		{
			name: "noconfigfile",
			args: args{configFileName: ""},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewGoBackupsBackend(tt.args.configFileName); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewGoBackupsBackend() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGoBackupsBackend_CheckIfConfigIsReadable(t *testing.T) {
	type fields struct {
		configFileName string
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name:   "config_test.json",
			fields: fields{configFileName: "../config_test.json"},
			want:   true,
		},
		{
			name:   "empty string",
			fields: fields{configFileName: ""},
			want:   false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &GoBackupsBackend{
				configFileName: tt.fields.configFileName,
			}
			if got := b.CheckIfConfigIsReadable(); got != tt.want {
				t.Errorf("GoBackupsBackend.CheckIfConfigIsReadable() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGoBackupsBackend_PrettyConfigFile(t *testing.T) {
	type fields struct {
		configFileName string
	}
	type args struct {
		indent string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bytes.Buffer
	}{
		{
			name:   "config_test.json",
			fields: fields{configFileName: "../config_test.json"},
			args:   args{indent: "x"},
			want: *bytes.NewBuffer([]byte{
				123, 10, 120, 34, 100, 101, 118, 105, 99, 101, 34, 58, 32, 91, 10,
				120, 120, 123, 10, 120, 120, 120, 34, 104, 111, 115, 116, 110, 97,
				109, 101, 34, 58, 32, 34, 104, 111, 115, 116, 110, 97, 109, 101,
				95, 116, 101, 115, 116, 34, 44, 10, 120, 120, 120, 34, 102, 111,
				108, 100, 101, 114, 34, 58, 32, 91, 10, 120, 120, 120, 120, 123,
				10, 120, 120, 120, 120, 120, 34, 110, 97, 109, 101, 34, 58, 32, 34,
				110, 97, 109, 101, 95, 116, 101, 115, 116, 34, 44, 10, 120, 120,
				120, 120, 120, 34, 112, 97, 116, 104, 34, 58, 32, 34, 112, 97, 116,
				104, 95, 116, 101, 115, 116, 34, 44, 10, 120, 120, 120, 120, 120,
				34, 98, 97, 99, 107, 117, 112, 76, 111, 99, 97, 116, 105, 111, 110,
				34, 58, 32, 34, 98, 97, 99, 107, 117, 112, 76, 111, 99, 97, 116,
				105, 111, 110, 95, 116, 101, 115, 116, 34, 10, 120, 120, 120, 120,
				125, 44, 10, 120, 120, 120, 120, 123, 10, 120, 120, 120, 120, 120,
				34, 110, 97, 109, 101, 34, 58, 32, 34, 110, 97, 109, 101, 95, 116,
				101, 115, 116, 50, 34, 44, 10, 120, 120, 120, 120, 120, 34, 112,
				97, 116, 104, 34, 58, 32, 34, 112, 97, 116, 104, 95, 116, 101, 115,
				116, 50, 34, 44, 10, 120, 120, 120, 120, 120, 34, 98, 97, 99, 107,
				117, 112, 76, 111, 99, 97, 116, 105, 111, 110, 34, 58, 32, 34, 98,
				97, 99, 107, 117, 112, 76, 111, 99, 97, 116, 105, 111, 110, 95, 116,
				101, 115, 116, 50, 34, 10, 120, 120, 120, 120, 125, 10, 120, 120,
				120, 93, 10, 120, 120, 125, 44, 10, 120, 120, 123, 10, 120, 120,
				120, 34, 104, 111, 115, 116, 110, 97, 109, 101, 34, 58, 32, 34,
				104, 111, 115, 116, 110, 97, 109, 101, 95, 116, 101, 115, 116, 50,
				34, 44, 10, 120, 120, 120, 34, 102, 111, 108, 100, 101, 114, 34,
				58, 32, 91, 10, 120, 120, 120, 120, 123, 10, 120, 120, 120, 120,
				120, 34, 110, 97, 109, 101, 34, 58, 32, 34, 110, 111, 109, 101, 95,
				116, 101, 115, 116, 34, 44, 10, 120, 120, 120, 120, 120, 34, 112,
				97, 116, 104, 34, 58, 32, 34, 112, 97, 116, 104, 95, 116, 101, 115,
				116, 34, 44, 10, 120, 120, 120, 120, 120, 34, 98, 97, 99, 107, 117,
				112, 76, 111, 99, 97, 116, 105, 111, 110, 34, 58, 32, 34, 98, 97,
				99, 107, 117, 112, 76, 111, 99, 97, 116, 105, 111, 110, 95, 116,
				101, 115, 116, 34, 10, 120, 120, 120, 120, 125, 10, 120, 120, 120,
				93, 10, 120, 120, 125, 10, 120, 93, 10, 125}),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &GoBackupsBackend{
				configFileName: tt.fields.configFileName,
			}
			if got := b.PrettyConfigFile(tt.args.indent); !reflect.DeepEqual(got.String(), tt.want.String()) {
				t.Errorf("GoBackupsBackend.PrettyConfigFile() = \n%v, want \n%v", got, tt.want)
			}
		})
	}
}
