package backend

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/garrettcornwell/gobackups/shared"
)

// GoBackupsBackend contains a configFileName that will be used to
// save configurations and decide which file(s) and folder(s) to backup
type GoBackupsBackend struct {
	configFileName string
}

// NewGoBackupsBackend creates a new GoBackupsBackend that will use
// the provided configFileName
func NewGoBackupsBackend(configFileName string) *GoBackupsBackend {
	if len(configFileName) < 1 {
		return nil
	}
	return &GoBackupsBackend{configFileName: configFileName}
}

// AddBackupToConfig adds the provided fodler to the config file
func (b *GoBackupsBackend) AddBackupToConfig(src string, dest string, name string) error {
	return nil
}

// AddDeviceToConfig adds the provided device to the current config
// file, if the provided device isn't already in the config file
func (b *GoBackupsBackend) AddDeviceToConfig(device string) error {
	return nil
}

// CreateConfigFile makes a new config file with the configFileName
func (b *GoBackupsBackend) CreateConfigFile() error {
	_, err := os.Open(b.configFileName)
	if err != nil {
		configFile, err := os.Create(b.configFileName)
		defer configFile.Close()
		if err != nil {
			return err
		}
		var config shared.ConfigJSON
		hostname, err := os.Hostname()
		if err != nil {
			return err
		}
		config.AddDevice(hostname)

		jsonConfig, err := json.MarshalIndent(config, "", "\t")
		if err != nil {
			return err
		}
		_, err = configFile.Write(jsonConfig)
		return err
	}
	return fmt.Errorf("config file already exists: %v", b.configFileName)
}

// FileCopy copies the src file to dst. Any existing file will be overwritten
// and will not copy file attributes
// source: https://stackoverflow.com/a/21061062
func FileCopy(src, dst string) error {
	in, err := ioutil.ReadFile(src)
	if err != nil {
		return err
	}

	out, err := os.OpenFile(dst, os.O_CREATE|os.O_EXCL|os.O_WRONLY, os.FileMode(0666))
	if err != nil {
		return err
	}
	defer out.Close()

	var config shared.ConfigJSON
	err = json.Unmarshal(in, &config)
	if err != nil {
		return err
	}

	jsonConfig, err := json.MarshalIndent(config, "", "\t")

	_, err = io.Copy(out, bytes.NewReader(jsonConfig))
	if err != nil {
		return err
	}
	return out.Close()
}

// BackupConfigFile makes a backsup of the configFileName and save it
// with a timestamp
func (b *GoBackupsBackend) BackupConfigFile() error {
	err := FileCopy(b.configFileName, strings.Join([]string{b.configFileName, strconv.FormatInt(time.Now().Unix(), 10), "bak"}, "."))
	return err
}

// CheckIfConfigIsReadable returns true if the config file
// is readable and false if the config file is unreadable
func (b *GoBackupsBackend) CheckIfConfigIsReadable() bool {
	_, err := ioutil.ReadFile(b.configFileName)
	if err != nil {
		filepath, filepatherr := filepath.Abs(b.configFileName)
		log.Println("ReadFile err: ", err, "\n", "absolute file path: ", filepath, "\n", "filepatherr: ", filepatherr)
		return false
	}
	return true
}

// PrettyConfigFile returns a pretty printed representation of
// the config file in a bytes.Buffer
func (b *GoBackupsBackend) PrettyConfigFile(indent string) bytes.Buffer {
	if indent == "" {
		indent = "\t"
	}
	configFile, err := ioutil.ReadFile(b.configFileName)
	if err != nil {
		log.Println(err)
	}
	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, configFile, "", indent)
	return prettyJSON
}

// RunBackups will read the input configFilename
// and run the backups specified in the config file
func (b *GoBackupsBackend) RunBackups() error {
	configFile, err := ioutil.ReadFile(b.configFileName)
	if err != nil {
		return err
	}

	var config shared.ConfigJSON
	err = json.Unmarshal(configFile, &config)

	hostname, err := os.Hostname()
	log.Println("Device hostname is: ", hostname)

	for _, device := range config.Device {
		if device.Hostname == hostname {
			b.runDeviceBackup(config, device.Hostname)
		}
	}
	return err
}

func (b *GoBackupsBackend) runDeviceBackup(config shared.ConfigJSON, hostname string) {
	for _, device := range config.Device {
		if device.Hostname == hostname {
			for _, folder := range device.Folder {
				source := filepath.Clean(folder.Path)
				destination := filepath.Clean(folder.BackupLocation)
				destination = filepath.Join(destination, strings.Join([]string{strconv.FormatInt(time.Now().Unix(), 10), "-", folder.Name, ".zip"}, ""))
				go b.backup(source, destination)
			}
		}
	}
}

func (b *GoBackupsBackend) backup(source string, destination string) {
	log.Println("backend.backup-source: ", source)
	log.Println("backend.backup-destination: ", destination)
	files := b.listFiles(source)

	err := b.zipMe(files, destination)
	if err != nil {
		log.Println("backend.backup-zipMe-err: ", err)
	}
}

func (b *GoBackupsBackend) listFiles(root string) []string {
	var files []string

	filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, path)
		}
		return nil
	})

	return files
}

func (b *GoBackupsBackend) zipMe(filepaths []string, target string) error {

	flags := os.O_WRONLY | os.O_CREATE | os.O_TRUNC
	file, err := os.OpenFile(target, flags, 0644)

	if err != nil {
		return fmt.Errorf("Failed to open zip for writing: %s", err)
	}
	defer file.Close()

	zipw := zip.NewWriter(file)
	defer zipw.Close()

	for _, filename := range filepaths {
		if err := b.addFileToZip(filename, zipw); err != nil {
			return fmt.Errorf("Failed to add file %s to zip: %s", filename, err)
		}
	}
	return err

}

func (b *GoBackupsBackend) addFileToZip(filename string, zipw *zip.Writer) error {
	file, err := os.Open(filename)

	if err != nil {
		return fmt.Errorf("Error opening file %s: %s", filename, err)
	}
	defer file.Close()

	wr, err := zipw.Create(filename)
	if err != nil {

		return fmt.Errorf("Error adding file; '%s' to zip : %s", filename, err)
	}

	if _, err := io.Copy(wr, file); err != nil {
		return fmt.Errorf("Error writing %s to zip: %s", filename, err)
	}

	return err
}
