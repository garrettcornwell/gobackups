package main

import (
	"log"

	"gitlab.com/garrettcornwell/gobackups/backend"

	"gitlab.com/garrettcornwell/gobackups/frontend"
)

func main() {
	configFilename := "config.json"
	port := 8080
	openBrowser := true
	backupNow := false

	gBackend := backend.NewGoBackupsBackend(configFilename)
	gFrontend := frontend.NewGoBackupsFrontend(port, gBackend)

	if backupNow {
		go log.Println(gBackend.RunBackups())
	}

	if openBrowser {
		go gFrontend.OpenFrontendInBrowser()
	}

	go log.Println(gFrontend.Start())
}
