package frontend

import (
	"io"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"time"

	"gitlab.com/garrettcornwell/gobackups/backend"
)

type GoBackupsFrontend struct {
	gbackend *backend.GoBackupsBackend
	port     int
}

func NewGoBackupsFrontend(port int, gbackend *backend.GoBackupsBackend) *GoBackupsFrontend {
	if port <= 0 {
		port = 8080
	}
	if gbackend == nil {
		return nil
	}
	return &GoBackupsFrontend{gbackend: gbackend, port: port}
}

func (f *GoBackupsFrontend) OpenFrontendInBrowser() {
	go openBrowser("http://localhost:" + strconv.Itoa(f.port))
}

// OpenBrowser opens the specified URL in the default browser.
func openBrowser(url string) error {
	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default: // "linux", "freebsd", "openbsd", "netbsd"
		cmd = "xdg-open"
	}
	args = append(args, url)
	return exec.Command(cmd, args...).Start()
}

func (f *GoBackupsFrontend) routes() {
	http.Handle("/", http.FileServer(AssetFile()))

	printConfigFunc := func(w http.ResponseWriter, _ *http.Request) {
		configFile := f.gbackend.PrettyConfigFile("")
		io.WriteString(w, string(configFile.Bytes()))
	}

	backupConfigFunc := func(w http.ResponseWriter, _ *http.Request) {
		err := f.gbackend.BackupConfigFile()
		if err != nil {
			io.WriteString(w, string("error backing up config file: "))
			io.WriteString(w, err.Error())
		} else {
			io.WriteString(w, string("backup complete"))
		}
	}

	createConfigFunc := func(w http.ResponseWriter, _ *http.Request) {
		err := f.gbackend.CreateConfigFile()
		if err != nil {
			io.WriteString(w, string("error creating config file: "))
			io.WriteString(w, err.Error())
		} else {
			io.WriteString(w, string("config file created"))
		}
	}

	runBackupFunc := func(w http.ResponseWriter, _ *http.Request) {
		f.gbackend.RunBackups()
		io.WriteString(w, "backup started")
	}

	exitFunc := func(w http.ResponseWriter, _ *http.Request) {
		io.WriteString(w, "<h1>goodbye</h1>")
		exit := func() {
			time.Sleep(1 * time.Second)
			os.Exit(1)
		}
		go exit()
	}

	restartFunc := func(w http.ResponseWriter, _ *http.Request) {
		io.WriteString(w, "<h1>restarting goBackups</h1>")
		exit := func() {
			time.Sleep(1 * time.Second)
			os.Exit(1)
		}
		go exit()
	}

	http.HandleFunc("/printConfig", printConfigFunc)
	http.HandleFunc("/backupConfig", backupConfigFunc)
	http.HandleFunc("/createConfig", createConfigFunc)
	http.HandleFunc("/runBackup", runBackupFunc)
	http.HandleFunc("/exit", exitFunc)
	http.HandleFunc("/restart", restartFunc)
}

// Start starts the HTML frontend
func (f *GoBackupsFrontend) Start() error {
	f.routes()
	return (http.ListenAndServe(":"+strconv.Itoa(f.port), nil))
}
