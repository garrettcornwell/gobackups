package shared

type folder struct {
	BackupLocation string `json:"backupLocation"`
	Name           string `json:"name"`
	Path           string `json:"path"`
}

type device struct {
	Hostname string   `json:"hostname"`
	Folder   []folder `json:"folder"`
}

// ConfigJSON is the golang structure representation of the config json file
// it is used to store configuration information about what should be backed up
// on each device
type ConfigJSON struct {
	Device []device `json:"device"`
}

// AddDevice adds the provided device hostname to the config file
func (c *ConfigJSON) AddDevice(hostname string) {
	c.Device = append(c.Device, device{Hostname: hostname})
}

// AddFolderToDevice adds the provided folder to the desired device
func (c *ConfigJSON) AddFolderToDevice(backupLocation string, name string, path string, hostname string) {
	for index, device := range c.Device {
		if device.Hostname == hostname {
			c.Device[index].Folder = append(c.Device[index].Folder, folder{BackupLocation: backupLocation, Name: name, Path: path})
		}
	}
}
