#!/usr/bin/env bash

go-bindata -o frontend/bindata.go -pkg frontend -fs -prefix "frontend/web" frontend/web/...
GOOS=linux GOARCH=386 go build -o goBackupsLinux386
GOOS=linux GOARCH=amd64 go build -race -o goBackupsLinuxAmd64
GOOS=windows GOARCH=386 go build -o goBackupsWindows386.exe
GOOS=windows GOARCH=amd64 go build -o goBackupsWindowsAmd64.exe
GOOS=darwin GOARCH=386 go build -o goBackupsDarwin386
GOOS=darwin GOARCH=amd64 go build -o goBackupsDarwinAmd64
go install